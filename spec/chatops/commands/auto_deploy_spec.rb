# frozen_string_literal: true

require 'spec_helper'

describe Chatops::Commands::AutoDeploy do
  let(:fake_client) { spy }

  let(:env) do
    [
      {},
      'SLACK_TOKEN' => 'token',
      'CHAT_CHANNEL' => 'channel',
      'GITLAB_TOKEN' => 'token',
      'GITLAB_OPS_TOKEN' => 'token',
      'CHEF_USERNAME' => 'bork',
      'CHEF_PEM_KEY' => 'bork'
    ]
  end

  before do
    stub_const('Chatops::Gitlab::Client', fake_client)
  end

  describe '.perform' do
    it 'includes examples in the --help output' do
      output = described_class.perform(%w[--help])

      expect(output).to include('Available subcommands:')
      expect(output).to include('Examples:')
    end
  end

  describe '#perform' do
    context 'when using a valid subcommand' do
      it 'executes the subcommand' do
        command = described_class.new(%w[status c01bc1930])

        expect(command)
          .to receive(:status)
          .with('c01bc1930')

        command.perform
      end
    end

    context 'when using an invalid subcommand' do
      it 'returns an error message' do
        command = described_class.new(%w[not_a_real_command])

        expect(command).to receive(:unsupported_command)

        command.perform
      end
    end
  end

  describe '.available_subcommands' do
    it 'returns a String' do
      expect(described_class.available_subcommands).to include('* status')
    end
  end

  describe '#pause' do
    let(:command) { described_class.new(%w[pause], *env) }

    it 'triggers a pause' do
      auto_deploy = instance_double('Chatops::Gitlab::AutoDeploy')
      expect(Chatops::Gitlab::AutoDeploy).to receive(:new)
        .with(fake_client)
        .and_return(auto_deploy)

      tasks = [
        instance_double('task', active: false, description: 'foo'),
        instance_double('task', active: false, description: 'bar')
      ]
      expect(auto_deploy).to receive(:pause).and_return(tasks)
      expect(command).to receive(:post_task_status)
        .with(tasks)
        .and_call_original

      expect_slack_message(blocks: TaskBlockMatcher.new(tasks))
      command.perform
    end
  end

  describe '#prepare', :release_command do
    it 'triggers `auto_deploy:prepare`' do
      instance = stubbed_instance('prepare')

      expect(instance).to receive(:trigger_release)
        .with(nil, 'auto_deploy:prepare')

      instance.perform
    end
  end

  describe '#tag', :release_command do
    it 'triggers `auto_deploy:tag' do
      instance = stubbed_instance('tag')

      expect(instance).to receive(:trigger_release).with(nil, 'auto_deploy:tag')

      instance.perform
    end
  end

  describe '#unpause' do
    let(:command) { described_class.new(%w[unpause], *env) }

    it 'triggers an unpause' do
      auto_deploy = instance_double('Chatops::Gitlab::AutoDeploy')
      expect(Chatops::Gitlab::AutoDeploy).to receive(:new)
        .with(fake_client)
        .and_return(auto_deploy)

      tasks = [
        instance_double(
          'task',
          active: true,
          description: 'foo',
          next_run_at: 'Tomorrow'
        ),
        instance_double(
          'task',
          active: true,
          description: 'bar',
          next_run_at: 'Next week'
        )
      ]
      expect(auto_deploy).to receive(:unpause).and_return(tasks)
      expect(command).to receive(:post_task_status)
        .with(tasks)
        .and_call_original

      expect_slack_message(blocks: TaskBlockMatcher.new(tasks))
      command.perform
    end
  end

  describe '#status' do
    let(:production_deployment_status) { 'success' }
    let(:production_status) do
      {
        role: 'gprd',
        version: '12.2.0-pre',
        revision: '0874a8d346c',
        sha: '0874a8d346c2c91fc78151a6bab004dd71d6bfba',
        branch: '12-2-auto-deploy-20190804',
        package: '12.2.201908042020-0874a8d346c.2ee9f1d280d',
        status: production_deployment_status
      }
    end

    context 'with no argument' do
      it 'send a formatted Slack message' do
        command = described_class.new(%w[status], *env)

        allow(command).to receive(:environment_status).and_return([production_status])
        expect_slack_message(blocks: StatusBlockMatcher.new(production_status))
        expect(command).not_to receive(:run_trigger).with(CHECK_PRODUCTION: 'true')

        command.perform
      end

      it 'includes production checks optionally' do
        local_env = env.dup
        local_env[0][:checks] = true

        command = described_class.new(%w[status], *local_env)

        allow(command).to receive(:environment_status).and_return([production_status])
        expect_slack_message(blocks: StatusBlockMatcher.new(production_status))
        expect(command).to receive(:run_trigger).with(CHECK_PRODUCTION: 'true', PRODUCTION_CHECK_SCOPE: 'deployment')

        command.perform
      end
    end

    context 'with a valid commit SHA' do
      let(:command) do
        described_class.new(%w[status abcdefg], *env)
      end

      before do
        allow(command).to receive(:environment_status)
          .and_return([production_status])
        allow(command).to receive(:auto_deploy_branches).with('abcdefg')
          .and_return([instance_double(
            'Branch', name: production_status[:branch]
          )])
      end

      it 'posts a message with deployed environments' do
        commits = [
          instance_double('Commit', id: '0874a8d346c2c91fc78151a6bab004dd71d6bfba'),
          instance_double('Commit', id: 'abcdefg')
        ]
        allow(fake_client).to receive(:commits)
          .and_return(commits)

        fake_commit = instance_double(
          'Commit',
          short_id: 'abcd',
          title: 'Commit title'
        )
        expect(fake_client).to receive(:commit).and_return(fake_commit)

        expect_slack_message(
          blocks: DeployedCommitBlockMatcher.new(production_status, fake_commit)
        )

        command.perform
      end

      it 'displays no deployed branch message if SHA is chronologically after the deployed commit' do
        commits = [
          instance_double('Commit', id: 'sha1'),
          instance_double('Commit', id: 'abcdefg'),
          instance_double('Commit', id: '0874a8d346c2c91fc78151a6bab004dd71d6bfba'),
          instance_double('Commit', id: 'sha3')
        ]
        allow(fake_client).to receive(:commits)
          .and_return(commits)

        expect_slack_message(blocks: NoDeployedBlockMatcher.new)

        command.perform
      end

      it 'displays deployed envs if SHA is chronologically before the deployed commit' do
        commits = [
          instance_double('Commit', id: 'sha1'),
          instance_double('Commit', id: 'sha2'),
          instance_double('Commit', id: '0874a8d346c2c91fc78151a6bab004dd71d6bfba'),
          instance_double('Commit', id: 'abcdefg')
        ]
        allow(fake_client).to receive(:commits)
          .and_return(commits)

        fake_commit = instance_double(
          'Commit',
          short_id: 'abcd',
          title: 'Commit title'
        )
        expect(fake_client).to receive(:commit).and_return(fake_commit)
        expect_slack_message(
          blocks: DeployedCommitBlockMatcher.new(production_status, fake_commit)
        )

        command.perform
      end

      it 'displays deployed envs if SHA is not in first page of commits in branch' do
        commits = [
          instance_double('Commit', id: 'sha1'),
          instance_double('Commit', id: 'sha2'),
          instance_double('Commit', id: '0874a8d346c2c91fc78151a6bab004dd71d6bfba')
        ]
        allow(fake_client).to receive(:commits)
          .and_return(commits)

        fake_commit = instance_double(
          'Commit',
          short_id: 'abcd',
          title: 'Commit title'
        )
        expect(fake_client).to receive(:commit).and_return(fake_commit)
        expect_slack_message(
          blocks: DeployedCommitBlockMatcher.new(production_status, fake_commit)
        )

        command.perform
      end

      it 'posts an warning message with no deployed environment' do
        allow(command).to receive(:environment_status).and_return({})
        allow(command).to receive(:auto_deploy_branches).with('abcdefg')
          .and_return([])
        expect_slack_message(blocks: NoDeployedBlockMatcher.new)

        command.perform
      end
    end

    context 'with multiple SHAs' do
      let(:command) { described_class.new(%w[status abcdefg hijkl], *env) }

      before do
        allow(command).to receive(:environment_status)
          .and_return([production_status])

        %w[abcdefg hijkl].each do |sha|
          allow(command).to receive(:auto_deploy_branches).with(sha)
            .and_return([instance_double(
              'Branch', name: production_status[:branch]
            )])
        end

        commits = [
          instance_double('Commit', id: '0874a8d346c2c91fc78151a6bab004dd71d6bfba'),
          instance_double('Commit', id: 'abcdefg'),
          instance_double('Commit', id: 'hijkl')
        ]
        allow(fake_client).to receive(:commits)
          .and_return(commits)
      end

      it 'checks each SHA' do
        fake_commit1 = instance_double(
          'Commit',
          short_id: 'abcd',
          title: 'Commit title 1'
        )
        fake_commit2 = instance_double(
          'Commit',
          short_id: 'hijk',
          title: 'Commit title 2'
        )
        expect(fake_client).to receive(:commit).and_return(fake_commit1, fake_commit2)

        message = instance_double('Chatops::Slack::Message')
        expect(Chatops::Slack::Message)
          .to receive(:new)
          .and_return(message)

        [fake_commit1, fake_commit2].each do |fake_commit|
          expect(message)
            .to receive(:send)
            .with(blocks: DeployedCommitBlockMatcher.new(production_status, fake_commit))
        end

        command.perform
      end
    end

    context 'with MR URL' do
      let(:command) { described_class.new(%w[status https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120710], *env) }
      let(:slack_message) { instance_double('Chatops::Slack::Message') }

      before do
        allow(command).to receive(:environment_status)
          .and_return([production_status])

        allow(fake_client)
          .to receive(:merge_request)
          .with('gitlab-org/gitlab', '120710')
          .and_return(
            instance_spy(
              'merge request',
              {
                merge_commit_sha: 'abcdefg',
                web_url: 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120710'
              }
            )
          )

        %w[abcdefg hijkl].each do |sha|
          allow(fake_client)
            .to receive(:commit_refs)
            .with('gitlab-org/security/gitlab', sha, include({ type: 'branch' }))
            .and_return(
              Gitlab::PaginatedResponse.new(
                [
                  instance_spy('Branch', { name: production_status[:branch] })
                ]
              )
            )
        end

        commits = [
          instance_double('Commit', { id: '0874a8d346c2c91fc78151a6bab004dd71d6bfba' }),
          instance_double('Commit', { id: 'abcdefg' }),
          instance_double('Commit', { id: 'hijkl' })
        ]
        allow(fake_client).to receive(:commits)
          .and_return(commits)

        fake_commit1 = instance_double(
          'Commit',
          {
            short_id: 'abcd',
            title: 'Commit title 1'
          }
        )
        fake_commit2 = instance_double(
          'Commit',
          {
            short_id: 'hijk',
            title: 'Commit title 1'
          }
        )
        allow(fake_client).to receive(:commit).and_return(fake_commit1, fake_commit2)

        allow(Chatops::Slack::Message)
          .to receive(:new)
          .and_return(slack_message)
      end

      it 'checks merge commit and searches for cherry-picked commits' do
        allow(fake_client)
          .to receive(:search_in_project)
          .with(
            'gitlab-org/security/gitlab',
            'commits',
            'cherry picked from commit abcdefg',
            production_status[:branch]
          )
          .and_return(
            [
              instance_spy('search results', id: 'hijkl', web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/commit/hijkl')
            ]
          )

        expect(Chatops::Slack::Message).to receive(:new)

        expected_blocks = slack_blocks(
          [
            '<https://gitlab.com/gitlab-org/security/gitlab/-/commit/abcd|`abcd`> Commit title 1',
            '<https://gitlab.com/gitlab-org/security/gitlab/-/commit/hijk|`hijk`> Commit title 1'
          ],
          [':party-tanuki: gprd']
        )

        expect(slack_message)
          .to receive(:send)
          .with(blocks: expected_blocks)

        expect(command.perform).to eq(
          'Merge commit: <https://gitlab.com/gitlab-org/security/gitlab/-/commit/abcdefg|abcdefg>, ' \
          'Cherry-picked commit: <https://gitlab.com/gitlab-org/security/gitlab/-/commit/hijkl|hijkl> ' \
          'for MR https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120710'
        )
      end

      it 'prints only merge commit when no cherry-picked commits are found' do
        allow(fake_client)
          .to receive(:search_in_project)
          .with(
            'gitlab-org/security/gitlab',
            'commits',
            'cherry picked from commit abcdefg',
            production_status[:branch]
          )
          .and_return([])

        expect(Chatops::Slack::Message).to receive(:new)

        expected_blocks = slack_blocks(
          ['<https://gitlab.com/gitlab-org/security/gitlab/-/commit/abcd|`abcd`> Commit title 1'],
          [':party-tanuki: gprd']
        )

        expect(slack_message)
          .to receive(:send)
          .with(blocks: expected_blocks)

        command.perform
      end
    end

    context 'with multiple MR URLs' do
      let(:command) do
        described_class.new(%w[status https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120710 something_else], *env)
      end

      it 'returns error message' do
        expect(fake_client).not_to receive(:merge_request)
        expect(fake_client).not_to receive(:search_in_project)

        expect(Chatops::Slack::Message).not_to receive(:new)

        expect(command.perform).to eq('This command accepts one MR URL argument')
      end
    end

    context 'with a running deployment' do
      let(:production_deployment_status) { 'running' }
      let(:command) do
        described_class.new(%w[status abcdefg], *env)
      end

      it 'ignores running deployments' do
        allow(command).to receive(:environment_status)
          .and_return([production_status])
        allow(command).to receive(:auto_deploy_branches).with('abcdefg')
          .and_return([instance_double(
            'Branch', name: production_status[:branch]
          )])

        fake_commit = instance_double(
          'Commit',
          short_id: 'abcd',
          title: 'Commit title'
        )
        expect(fake_client).to receive(:commit).and_return(fake_commit)

        expect_slack_message(blocks: NoDeployedBlockMatcher.new)

        command.perform
      end
    end

    context 'with an invalid commit SHA' do
      let(:command) do
        described_class.new(%w[status abcdefg], *env)
      end

      before do
        allow(fake_client).to receive(:commit)
          .and_raise(gitlab_error(:NotFound))

        allow(command).to receive(:chef_client).and_return(
          instance_double('Chatops::Chef::Client', package_version: 'foo')
        )
      end

      it 'posts an error message' do
        expect_slack_message(blocks: InvalidCommitBlockMatcher.new('abcdefg'))

        command.perform
      end
    end

    describe '#promotable_env_revision' do
      let(:production_rev) { 'abc1234' }
      let(:canary_rev)     { 'fff1235' }
      let(:staging_rev)    { 'ddd1236' }
      let(:envs) do
        [
          { revision: production_rev },
          { revision: canary_rev },
          { revision: staging_rev }
        ]
      end

      def promotable_env_revision(index)
        subject.send(:promotable_env_revision, envs, index)
      end

      it 'returns nil for the last env' do
        expect(promotable_env_revision(2)).to be_nil
      end

      it 'returns the revision of the next env' do
        expect(promotable_env_revision(0)).to match('abc1234...fff1235')
        expect(promotable_env_revision(1)).to match('fff1235...ddd1236')
      end

      it 'swaps the order for a running deployment' do
        upcoming = { revision: 'eee1234', status: 'running' }

        envs.unshift(upcoming)

        expect(promotable_env_revision(0)).to match('abc1234...eee1234')
      end

      it 'returns nil for index out of bounds' do
        expect(promotable_env_revision(100)).to be_nil
      end
    end
  end

  describe '#security_status' do
    def merge_request_stub(values = {})
      # HACK: `stringify_keys` without requiring ActiveSupport
      values = JSON.parse(values.to_json)

      Gitlab::ObjectifiedHash.new(values)
    end

    it 'reports undeployed merge requests' do
      command = described_class.new(%w[security_status], *env)

      merged = merge_request_stub(web_url: 'example.com/mr/1', references: { full: 'foo/bar!1' })
      deployed = merge_request_stub(web_url: 'example.com/mr/2', references: { full: 'foo/bar!2' })

      expect(fake_client).to receive(:merge_requests)
        .with(anything, hash_excluding(environment: 'gprd'))
        .and_return([merged, deployed])

      expect(fake_client).to receive(:merge_requests)
        .with(anything, hash_including(environment: 'gprd'))
        .and_return([deployed])

      expect_slack_message(blocks: SecurityStatusBlockMatcher.new(merged.web_url))

      command.perform
    end

    it 'ignores removed mr' do
      command = described_class.new(%w[security_status], *env)

      mr_merged1 = merge_request_stub(iid: 3799, web_url: 'example.com/mr/3799', references: { full: 'foo/bar!3799' })
      mr_merged2 = merge_request_stub(web_url: 'example.com/mr/1', references: { full: 'foo/bar!1' })

      expect(fake_client).to receive(:merge_requests)
        .with(anything, hash_excluding(environment: 'gprd'))
        .and_return([mr_merged1, mr_merged2])

      expect(fake_client).to receive(:merge_requests)
        .with(anything, hash_including(environment: 'gprd'))
        .and_return([mr_merged2])

      expect(Chatops::Slack::Message)
        .not_to receive(:new)

      command.perform
    end
  end

  describe '#blockers' do
    let(:command) { described_class.new([], *env) }

    it 'triggers a release-tools production check' do
      expect(command).to receive(:run_trigger).with(CHECK_PRODUCTION: 'true', PRODUCTION_CHECK_SCOPE: 'deployment')

      expect(command.blockers)
        .to eq('Production checks triggered, the results will appear shortly.')
    end
  end

  describe '#lock' do
    let(:auto_deploy) { instance_double('Chatops::Gitlab::AutoDeploy') }

    before do
      allow(Chatops::Gitlab::AutoDeploy)
        .to receive(:new)
        .with(fake_client)
        .and_return(auto_deploy)
    end

    context 'when no custom branch is given' do
      it 'locks deploys to the deployed auto-deploy branch' do
        command = described_class.new([], *env)

        allow(command)
          .to receive(:environment_status)
          .with('gprd')
          .and_return(branch: 'foo')

        expect(auto_deploy).to receive(:pause_prepare)

        expect(fake_client)
          .to receive(:update_variable)
          .with('gitlab-org/release/tools', 'AUTO_DEPLOY_BRANCH', 'foo')

        command.lock
      end
    end

    context 'when a custom branch is given' do
      it 'locks deploys to the given branch' do
        command = described_class.new([], *env)

        expect(auto_deploy).to receive(:pause_prepare)

        expect(fake_client)
          .to receive(:update_variable)
          .with('gitlab-org/release/tools', 'AUTO_DEPLOY_BRANCH', 'foo')

        expect(command.lock('foo'))
          .to eq('Auto deploys have been locked to branch foo')
      end
    end
  end

  describe '#unlock' do
    it 'resumes the preparing of auto-deploy branches' do
      command = described_class.new([], *env)
      auto_deploy = instance_double('Chatops::Gitlab::AutoDeploy')

      allow(Chatops::Gitlab::AutoDeploy)
        .to receive(:new)
        .with(fake_client)
        .and_return(auto_deploy)

      expect(auto_deploy).to receive(:unpause_prepare)

      command.unlock
    end
  end

  describe '#package_link' do
    context 'with valid package' do
      context 'with auto-deploy package' do
        it 'returns URL to omnibus-gitlab security mirror for correct tag' do
          command = AutoDeployTestForPackageLink.new([], *env)
          expect(command.package_link('14.1.202107120320-592cabc6d0d.a261be1cc84')).to eq('<https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/commits/14.1.202107120320+592cabc6d0d.a261be1cc84|`14.1.202107120320-592cabc6d0d.a261be1cc84`>')
        end
      end

      context 'with RC package' do
        it 'returns URL to omnibus-gitlab security mirror for correct tag' do
          command = AutoDeployTestForPackageLink.new([], *env)
          expect(command.package_link('13.7.0-rc3.ee.0')).to eq('<https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/commits/13.7.0+rc3.ee.0|`13.7.0-rc3.ee.0`>')
        end
      end

      context 'with regular release package' do
        it 'returns URL to omnibus-gitlab security mirror for correct tag' do
          command = AutoDeployTestForPackageLink.new([], *env)
          expect(command.package_link('14.0.0-ee.0')).to eq('<https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/commits/14.0.0+ee.0|`14.0.0-ee.0`>')
        end
      end
    end

    context 'with empty package' do
      it 'returns Unknown' do
        command = AutoDeployTestForPackageLink.new([], *env)
        expect(command.package_link(nil)).to eq('Unknown')
      end
    end
  end
end

# RSpec argument matcher for verifying the complex `block` Hash passed to
# `Slack::Message#send` from the described class
class StatusBlockMatcher
  def initialize(status)
    @status = status
  end

  def ===(other)
    json = other.to_json

    json.include?(':party-tanuki:') &&
      json.include?(@status[:revision]) &&
      json.include?(@status[:branch]) &&
      json.include?(@status[:package])
  end
end

class SecurityStatusBlockMatcher
  def initialize(expected)
    @expected = expected
  end

  def ===(other)
    json = other.to_json

    json.include?(':warning:') && json.include?(@expected)
  end
end

class DeployedCommitBlockMatcher
  def initialize(status, commit, deployed_envs = ['gprd'])
    @status = status
    @commit = commit
    @deployed_envs = deployed_envs
  end

  def ===(other)
    json = other.to_json

    json.include?("`#{@commit.short_id}`") &&
      json.include?(@commit.title) &&
      @deployed_envs.all? { |env| json.include?(env) }
  end
end

class NoDeployedBlockMatcher
  def ===(other)
    other.to_json.include?(':warning: Unable to find a deployed branch')
  end
end

class InvalidCommitBlockMatcher
  def initialize(commit_sha)
    @commit_sha = commit_sha
  end

  def ===(other)
    other.to_json.include?(":exclamation: `#{@commit_sha}` not found")
  end
end

class TaskBlockMatcher
  def initialize(tasks)
    @tasks = tasks
  end

  def ===(other)
    json = other.to_json

    @tasks.all? do |task|
      icon = task.active ? ':white_check_mark:' : ':double_vertical_bar:'
      json.include?(icon) &&
        json.include?(task.description) &&
        json.include?(summary)
    end
  end

  private

  def summary
    if @tasks.all?(&:active)
      'Scheduled auto-deploy tasks have been re-enabled.'
    else
      'Scheduled auto-deploy tasks have been temporarily disabled.'
    end
  end
end

class AutoDeployTestForPackageLink < Chatops::Commands::AutoDeploy
  def package_link(package = Chatops::Commands::AutoDeploy.new)
    super package
  end
end

def slack_blocks(section_texts, context_texts)
  blocks =
    section_texts.map do |text|
      {
        text: {
          text: text,
          type: 'mrkdwn'
        },
        type: 'section'
      }
    end

  elements =
    context_texts.map do |text|
      {
        text: text,
        type: 'mrkdwn'
      }
    end

  blocks << { elements: elements, type: 'context' }
end
